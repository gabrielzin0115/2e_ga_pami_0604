import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-serie',
  templateUrl: './detalhes-serie.page.html',
  styleUrls: ['./detalhes-serie.page.scss'],
})
export class DetalhesSeriePage {
  
  titulo = "Police Series App";
  series =[ 
  {
    titulo: "Brooklyn Nine-Nine",
    subtitulo: "",
    capa: "https://www.jornaldevalinhos.com.br/wp-content/uploads/2020/10/80381490_2443333819314988_3334100787302523140_n-640x640.jpg",
    texto: "Brooklyn Nine-Nine (abreviado como B99) é uma série de televisão de comédia policial americana criada por Dan Goor e Michael Schur. A série gira em torno de Jake Peralta (Andy Samberg), um imaturo, mas talentoso, detetive da polícia de Nova York na fictícia 99.ª Delegacia do Brooklyn, que muitas vezes entra em conflito com seu novo comandante, o sério e severo capitão Raymond Holt (Andre Braugher)."
  }
  ];
  constructor() {}
}
